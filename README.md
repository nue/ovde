# ovde
Media library organizer written in Ruby.

## Usage
First, install the following gems:
```sh
gem install zaru pastel ruby-taglib
```

```sh
ovde # [options ...]
```

Available options:
- `dry`: Don't execute sensible actions.
- `copy`: Copy files instead of moving them.
- `force`: Force library creation.

Run first with `dry` option to make sure the script won't mess up your library.
I've tried to minimize any wrong behaviour, but it may still happen.

```sh
ovde dry
ovde force
```

Make sure to change the variable `root_dir` (located in line 13) to where your music is stored,
and `nlib_dir` (located in line 14) to where you'd like your library to reside in.
If you'd like to change the library's name, change in line 15 `"music.lib"` to whatever you find more convenient

If you need to accept other audio extensions, add them to line 23, like this:
```ruby
files = Dir.glob("**/*.{flac,mp3,wav,aac,aiff,etc}")
```

I may change this functionality to an array containing the extensions soon.

Line 100 specifies how files will be named when organized into the library.
The default is `<number> <title>.<extension>`, but feel free to change it to any format you may find convenient,
for example, to use `<number>. <artist> - <title>.<extension>` as the format, do the following:

```ruby
name = Zaru.sanitize!(truncate("#{'%02d' % track[:track_number]}. #{artist} - #{track[:title]}#{track[:extension]}", 64))
```

## Testing
This script has been tested in Linux, but it should probably work with macOS and most *BSDs
without much change needed.

I have no plans on supporting Windows, but feel free to change whatever it's needed to make it work.

## Roadmap
- [ ] Organize cover images and such. (working in **next** branch)
- [ ] Modularize functionality. (coming soon) 
- [ ] Finish regex tag heuristics.

## License
All material in this repository is in the public domain.
